-r base.txt
elasticsearch==5.5.3

django_cache_url==3.0.0
django-redis==4.11.0
dj-database-url==0.5.0
django-redis==4.9.0
gevent==1.3.3
greenlet==0.4.13
gunicorn==19.8.1
psycopg2>=2.7,<3.0
uwsgi>=2.0.17,<2.1
whitenoise>=5.0,<5.1

# Additional dependencies for Heroku deployment
boto3==1.9.189
django-storages>=1.8,<1.9
# For retrieving credentials and signing requests to Elasticsearch
botocore>=1.12.33,<1.13
aws-requests-auth==0.4.1
# For copying initial media to S3 bucket
awscli==1.15.39
